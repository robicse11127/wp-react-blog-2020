import React, { createContext, useState, useEffect } from 'react'
import axios from 'axios'
import config from '../Config'

export const PostsContext = createContext();

const PostsContextProvider = ( props ) => {

    const [ posts, setPosts ] = useState();
    const [ meta, setMeta ] = useState();
    const [ params, setParams ] = useState({
        per_page: 10,
        offset: 0
    })

    const loadMore = () => {
        params.offset += params.per_page
        setParams({
            per_page: params.per_page,
            offset: params.offset
        })
    }

    /**
     * Fetching Data
     */
    useEffect( () => {
        axios.get( `${config.apiURL}/posts`, { params } )
        .then( ( res ) => {
            setPosts( res.data )
            setMeta( res.headers )
        } )
    }, [params] )

    /**
     * Return Provider
     */
    return(
        <PostsContext.Provider value={ { posts, meta, loadMore } }>
            { props.children }
        </PostsContext.Provider>
    )

}

export default PostsContextProvider;