import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { fade, makeStyles } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'

const useStyles = makeStyles( ( theme ) => ({
    root: {
        padding: '0',
        width: '100%'
    },
    pageHeader: {
        backgroundColor: '#153e90',
        height: '400px',
        textAlign: 'center',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontWeight: '700',
        color: '#fff'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade( theme.palette.common.white, 0.15 ),
        '&:hover': {
            backgroundColor: fade( theme.palette.common.white, 0.25 ),
        },
        marginLeft: 0,
        width: '100%',
        [ theme.breakpoints.up( 'sm' ) ]: {
            marginLeft: theme.spacing( 1 ),
            width: 'auto'
        },
    },
    searchIcon: {
        padding: theme.spacing( 0, 2 ),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit'
    },
    inputInput: {
        padding: theme.spacing( 1, 1, 1, 0 ),
        paddingLeft: `calc( 1em + ${theme.spacing( 4 )}px )`,
        transition: theme.transitions.create( 'width' ),
        width: '100%',
        [ theme.breakpoints.up( 'sm' ) ]: {
            width: '12ch',
            '&:focus': {
                width: '20ch'
            }
        }
    },
}) )

export default function PageHeader() {
    const classes = useStyles()
    return(
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="xl" className={ classes.root }>
                <div className={ classes.pageHeader }>
                    <Typography variant="h2" className={ classes.title }>
                        WordPress with <br /> React JS & Material UI
                    </Typography>
                </div>

            </Container>
        </React.Fragment>
    )
}