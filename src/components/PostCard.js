import React, { useState, useEffect, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import Grid from '@material-ui/core/Grid'
import { Button } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import renderHTML from 'react-render-html'

import { PostsContext } from '../contexts/PostsContext'

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '100px 0px',
        maxWidth: '1170px',
        margin: 'auto'
    },
    card: {
        margin: '30px'
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    avatar: {
        backgroundColor: red[500],
    },
    titleLink: {
        color: '#303030',
        textDecoration: 'none',
        lineHeight: '36px',
        margin: '12px 0px',
        display: 'block',
        fontWeight: '600'
    },
    readmore: {
        marginTop: '15px',
    },
    sectionTitle: {
        paddingLeft: '30px',
        display: 'block',
        position: 'relative',
        zIndex: '0',
        '&::after': {
            content: '',
            position: 'absolute',
            width: '50px',
            height: '3px',
            background: '#222',
            right: '-50px',
            top: '50%',
            zIndex: '10,'
        }
    }
}));

const Posts = () => {
    const classes = useStyles();

    const [ items, setItems ] = useState([]);

    /**
     * Destructure PostsContext
     */
    const { posts, meta, loadMore } = useContext( PostsContext )

    const limitWords = ( content, max ) => {
        return content.substr( 0, max ) + '...';
    }

    if( undefined !== posts ) {
        let newItems = [...items];
        newItems.push( posts )
        setItems( { items: newItems } )
        console.log( items )
    }

    return (
        <>
            <Grid className={ classes.root } container direction="row" justify="center">
                <Grid item xs={ 12 } md={ 6 }>
                    <Typography variant="h2" component="h2" className={ classes.sectionTitle }>Latest Articles</Typography>
                </Grid>
                <Grid item xs={ 12 } sm={ 12 } md={ 6 }>
                    <Typography >This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like</Typography>
                </Grid>
                {  undefined !== posts &&
                    // Looping through posts
                    items.map( ( post, index ) => {
                        return(
                            <Grid item xs={ 6 } md={ 6 } key={ post.id }>
                                <Card className={classes.card}>
                                    <CardMedia
                                        className={classes.media}
                                        image={ post.featured_image_src.full }
                                        title={ post.slug }
                                    />
                                    <CardContent>
                                        <Typography variant="h4" component="h2">
                                            <a className={ classes.titleLink } href={ post.link }>{ post.title.rendered }</a>
                                        </Typography>
                                        <Typography variant="body1" color="textSecondary" component="p">
                                            { renderHTML( limitWords( post.excerpt.rendered, 100 ) ) }
                                        </Typography>
                                        <Button className={ classes.readmore } variant="outlined" color="primary" href={ post.link }>Read More</Button>
                                    </CardContent>
                                </Card>
                            </Grid>
                        )
                    } )
                }

                <Grid className={ classes.loadMoreContainer } container direction="row" justify="center">
                    <Button variant="outlined" color="primary" onClick={ loadMore }>Load More Articles</Button>
                </Grid>
            </Grid>
        </>
    );
}

export default Posts;