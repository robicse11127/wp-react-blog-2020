import React from 'react'
import { fade, makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import ToolBar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'

const useStyles = makeStyles( ( theme ) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing( 2 ),
    },
    title: {
        flexGrow: 1,
        display: 'none',
        [ theme.breakpoints.up( 'sm' ) ]: {
            display: 'block'
        }
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade( theme.palette.common.white, 0.15 ),
        '&:hover': {
            backgroundColor: fade( theme.palette.common.white, 0.25 ),
        },
        marginLeft: 0,
        width: '100%',
        [ theme.breakpoints.up( 'sm' ) ]: {
            marginLeft: theme.spacing( 1 ),
            width: 'auto'
        }
    },
    searchIcon: {
        padding: theme.spacing( 0, 2 ),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit'
    },
    inputInput: {
        padding: theme.spacing( 1, 1, 1, 0 ),
        paddingLeft: `calc( 1em + ${theme.spacing( 4 )}px )`,
        transition: theme.transitions.create( 'width' ),
        width: '100%',
        [ theme.breakpoints.up( 'sm' ) ]: {
            width: '12ch',
            '&:focus': {
                width: '20ch'
            }
        }
    },
    navMenus: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    navItemLists: {
        width: '100%',
        display: 'inline-block',
        padding: '0',
        margin: '0',
    },
    navItem: {
        display: 'inline-block',
    },
    navLink: {
        display: 'block',
        padding: '7px 20px',
        color: 'white',
        fontSize: '16px',
        fontWeight: '500',
        textDecoration: 'none'
    }
}) )

export default function Navigation() {
    const classes = useStyles();

    return(
        <div className={ classes.root }>
            <AppBar position="static">
                <ToolBar>
                    <IconButton edge="start" className={ classes.menuButton } color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={ classes.title }>
                        WP React App
                    </Typography>
                    <div className={ classes.navMenus }>
                        <ul className={ classes.navItemLists }>
                            <li className={ classes.navItem }><a href="" className={ classes.navLink }>Home</a></li>
                            <li className={ classes.navItem }><a href="" className={ classes.navLink }>Blog</a></li>
                            <li className={ classes.navItem }><a href="" className={ classes.navLink }>About</a></li>
                            <li className={ classes.navItem }><a href="" className={ classes.navLink }>Contact</a></li>
                        </ul>
                    </div>
                    <div className={ classes.search }>
                        <div className={ classes.searchIcon }>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search..."
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </ToolBar>
            </AppBar>
        </div>
    )
}