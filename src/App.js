import './App.css';
import Navigation from './components/Navigation'
import PageHeader from './components/PageHeader'
import PostCards from './components/PostCard'

import PostsContextProvider from './contexts/PostsContext'
function App() {
    return (
        <div className="App">
            <Navigation />
            <PageHeader />
            <PostsContextProvider>
                <PostCards />
            </PostsContextProvider>
        </div>
    );
}
export default App;
