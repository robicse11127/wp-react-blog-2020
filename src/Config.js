export default {
    baseURL: ( process.env.NODE_ENV === 'development' ) ? 'http://localhost:3000' : '',
    apiURL: ( process.env.NODE_ENV === 'development' ) ? 'http://wpdev.test/wp-json/wp/v2' : '',
    appURL: ( process.env.NODE_ENV === 'development' ) ? 'http://wpdev.test' : ''
}